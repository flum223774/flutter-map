class Direction {
  int distanceValue;
  int durationValue;
  String distanceText;
  String durationText;
  String encodedPoints;

  Direction(
      {required this.distanceValue,
      required this.durationValue,
      required this.distanceText,
      required this.durationText,
      required this.encodedPoints});
  @override
  String toString() {
    // TODO: implement toString
    return distanceText;
  }
}
