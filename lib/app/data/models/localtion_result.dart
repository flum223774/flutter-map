import 'package:google_maps_flutter/google_maps_flutter.dart';

class LocationResult {
  String name;
  String locality;
  LatLng latLng;
  String formattedAddress;
  String placeId;

  LocationResult(
      {required this.name,
      required this.locality,
      required this.latLng,
      required this.formattedAddress,
      required this.placeId});

  @override
  String toString() {
    return '${this.name}, ${this.locality}';
  }
}
