import 'package:flutter_map/app/modules/home/controllers/draw_map_controller.dart';
import 'package:get/get.dart';

class DrawBinding extends Bindings{
  @override
  void dependencies() {
    Get.put<DrawMapController>(DrawMapController());
  }
}