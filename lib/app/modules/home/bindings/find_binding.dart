import 'package:flutter_map/app/modules/home/controllers/find_map_controller.dart';
import 'package:get/get.dart';

class FindBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<FindMapController>(FindMapController());
  }
}
