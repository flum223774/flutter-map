import 'package:flutter_map/app/modules/home/providers/search_provider.dart';
import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<SearchProvider>(SearchProvider());
    Get.put<HomeController>(HomeController());
  }
}
