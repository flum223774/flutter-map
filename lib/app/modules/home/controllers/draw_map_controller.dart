import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_map/app/configs/app.dart';
import 'package:flutter_map/app/data/models/localtion_result.dart';
import 'package:flutter_map/app/modules/home/providers/search_provider.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class DrawMapController extends GetxController {
  SearchProvider _provider = Get.find();
  Completer<GoogleMapController> controllerGoogleMap = Completer();
  late GoogleMapController newGoogleMapController;

  List<LatLng> _pLineCoordinates = [];
  RxSet<Polyline> polylineSet = RxSet();

  // RxMap<PolylineId, Polyline> polyLines = RxMap();

  late Position currentPosition;
  Rxn<LocationResult> pickPosition = Rxn();
  var geoLocator = Geolocator();

  @override
  void onInit() {
    super.onInit();
  }

  void locatePosition() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    currentPosition = position;
    LatLng latLngPosition = LatLng(position.latitude, position.longitude);
    moveToLocation(latLngPosition);
    LocationResult? locationResult = await _provider
        .reverseGeocodeLatLng(LatLng(position.latitude, position.longitude));
    print("this is your address" + locationResult.toString());
  }

  moveToLocation(LatLng latLngPosition) {
    newGoogleMapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: latLngPosition,
          zoom: 17,
        ),
      ),
    );
  }

  setPickedLocation(LocationResult toLocation) {
    pickPosition(toLocation);
  }

  Future<void> drawDirections() async {
    if (pickPosition.value == null) return;
    var fromLocation =
        PointLatLng(currentPosition.latitude, currentPosition.longitude);
    var toLocation =
        PointLatLng(pickPosition.value!.latLng.latitude, pickPosition.value!.latLng.longitude);

    print('From Location ${fromLocation.toString()}');
    print('To Location ${toLocation.toString()}');
    await _getPolyline(fromLocation, toLocation);
  }

  _addPolyLine() {
    print("ADD LINE");
    polylineSet.clear();
    Polyline polyline = Polyline(
      polylineId: PolylineId("polylineId"),
      color: Colors.pink,
      jointType: JointType.round,
      points: _pLineCoordinates,
      width: 5,
      startCap: Cap.roundCap,
      endCap: Cap.roundCap,
      geodesic: true,
    );
    polylineSet.add(polyline);
    print("OK");
  }

  _getPolyline(PointLatLng from, PointLatLng to) async {
    print("Get Polyline ");
    //Xoá line cũ
    _pLineCoordinates.clear();
    PolylinePoints polylinePoints = PolylinePoints();
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
        AppConfig.API_KEY, from, to,
        travelMode: TravelMode.driving);
    print(result.points);
    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        _pLineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
      _addPolyLine();
      _updateMap(LatLng(from.latitude, from.longitude),
          LatLng(to.latitude, to.longitude));
    }
  }

  _updateMap(LatLng from, LatLng to) {
    print('UPDate MAP');
    LatLngBounds latLngBounds;
    if (from.latitude > to.latitude && from.longitude > to.longitude) {
      latLngBounds = LatLngBounds(southwest: to, northeast: from);
    } else if (from.longitude > to.longitude) {
      latLngBounds = LatLngBounds(
          southwest: LatLng(from.latitude, to.longitude),
          northeast: LatLng(to.latitude, from.longitude));
    } else if (from.latitude > to.latitude) {
      latLngBounds = LatLngBounds(
          southwest: LatLng(to.latitude, from.longitude),
          northeast: LatLng(from.latitude, to.longitude));
    } else {
      latLngBounds = LatLngBounds(southwest: from, northeast: to);
    }

    newGoogleMapController
        .animateCamera(CameraUpdate.newLatLngBounds(latLngBounds, 70));
    print('UPDate MAP Done');
  }
}
