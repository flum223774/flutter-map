import 'dart:async';

import 'package:flutter_map/app/data/models/localtion_result.dart';
import 'package:flutter_map/app/modules/home/providers/search_provider.dart';
import 'package:flutter_map/app/widgets/search_overlay.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';

class FindMapController extends GetxController {
  final SearchProvider _provider = Get.find();
  final Completer<GoogleMapController> mapController = Completer();
  RxSet<Marker> markers = RxSet<Marker>();
  String previousSearchTerm = '';
  RxBool hasSearchTerm = false.obs;

  Rxn<LocationResult> locationResult = Rxn<LocationResult>();

  void onMapCreated(GoogleMapController controller) {
    mapController.complete(controller);
  }

  Future<void> searchPlace(String? keyword) async {
    if (keyword != null && keyword.length <= 3) {
      return;
    }
    if (keyword == this.previousSearchTerm) {
      return;
    } else {
      previousSearchTerm = keyword!;
    }
    hideAutoCompleteSuggestions();
    showAutoCompleteSearch();
    List<AutoCompleteItem> items = await _provider.autoCompleteSearch(keyword);
    var suggestions = items
        .map((aci) => RichSuggestion(aci, () {
              FocusScope.of(Get.context!).requestFocus(FocusNode());
              selectAutoCompleteItem(aci.id!);
            }))
        .toList();
    hideAutoCompleteSuggestions();
    showAutoCompleteSuggestions(suggestions);
  }

  void selectAutoCompleteItem(String id) async {
    var latLng = await _provider.decodeAndSelectPlace(id);
    if (latLng != null) moveToLocation(latLng);
  }

  Future<void> moveToLocation(LatLng latLng) async {
    hideAutoCompleteSuggestions();
    mapController.future.then((controller) {
      controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            target: latLng,
            zoom: 15.0,
          ),
        ),
      );
    });

    setMarker(latLng);

    locationResult.value = await _provider.reverseGeocodeLatLng(latLng);
    hasSearchTerm(locationResult.value != null);
  }

  void setMarker(LatLng latLng) {
    // markers.clear();
    markers.clear();
    markers.add(
      Marker(
        markerId: MarkerId("selected-location"),
        position: latLng,
      ),
    );
  }

  String getLocationName() {
    if (locationResult.value == null) {
      return "Unnamed location";
    }
    return "${locationResult.value!.name}, ${this.locationResult.value!.locality}";
  }

  @override
  void onClose() {
    hideAutoCompleteSuggestions();
  }
}
