import 'dart:async';

import 'package:flutter_map/app/data/models/direction.dart';
import 'package:flutter_map/app/modules/home/providers/search_provider.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class HomeController extends GetxController {
  SearchProvider _provider = Get.find();
  Rxn<Position> position = Rxn<Position>();
  Rxn<LatLng> positionPick = Rxn<LatLng>();
  StreamSubscription<Position>? _positionStreamSubscription;
  RxDouble distance = 0.0.obs;
  RxInt distanceAPI = 0.obs;

  // Stream<Position> get locationListen =>  Geolocator.getPositionStream();

  @override
  void onInit() {
    super.onInit();
    // Tự động lấy update location
    // position.bindStream(Geolocator.getPositionStream());
    _ListenPosition();
  }

  _ListenPosition() {
    if (_positionStreamSubscription == null) {
      final positionStream = Geolocator.getPositionStream();
      _positionStreamSubscription = positionStream.handleError((error) {
        _positionStreamSubscription?.cancel();
        _positionStreamSubscription = null;
      }).listen((newPosition) => {position(newPosition)});
      _positionStreamSubscription?.pause();
    }
  }

  void toggleListen() {
    print(_positionStreamSubscription);
    if (_positionStreamSubscription == null) {
      return;
    }
    if (_positionStreamSubscription!.isPaused) {
      _positionStreamSubscription!.resume();
    } else {
      _positionStreamSubscription!.pause();
    }
  }

  void setPickLocation(LatLng pick) {
    positionPick(pick);
  }

  Future<void> distanceCalculator() async {
    //đường chim bay ==> Giá trị trả ra là mét
    distance(await Geolocator.distanceBetween(
        position.value!.latitude,
        position.value!.longitude,
        positionPick.value!.latitude,
        positionPick.value!.longitude));
  }

  Future<void> distanceAPICalculator() async {
    if (position.value == null || positionPick.value == null) return;
    Direction? direction = await _provider.findDirection(
        LatLng(position.value!.latitude, position.value!.longitude),
        LatLng(positionPick.value!.latitude, positionPick.value!.longitude));

    print('direction ${direction.toString()}');
    if (direction != null) {
      distanceAPI(direction.distanceValue);
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}
