import 'package:flutter_map/app/configs/app.dart';
import 'package:flutter_map/app/data/models/direction.dart';
import 'package:flutter_map/app/data/models/localtion_result.dart';
import 'package:flutter_map/app/widgets/search_overlay.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:uuid/uuid.dart';

class SearchProvider extends GetConnect {
  // https://developers.google.com/maps/documentation/places/web-service/autocomplete
  String sessionToken = Uuid().v4();

  Future<List<AutoCompleteItem>> autoCompleteSearch(String place) async {
    List<AutoCompleteItem> items = [];
    place = place.replaceAll(" ", "+");
    var endpoint =
        "https://maps.googleapis.com/maps/api/place/autocomplete/json?" +
            "key=${AppConfig.API_KEY}&" +
            "language=vi&" +
            "components=country:vn&" +
            "input={$place}&sessiontoken=${this.sessionToken}";

    // if (locationResult != null) {
    //   endpoint += "&location=${locationResult!.latLng.latitude}," +
    //       "${locationResult!.latLng.longitude}";
    // }
    await get(endpoint).then((response) {
      if (response.statusCode == 200) {
        Map<String, dynamic> data = response.body;
        List<dynamic> predictions = data['predictions'];
        if (predictions.isEmpty) {
          items.add(
              AutoCompleteItem(text: "No result found", offset: 0, length: 0));
        } else {
          for (dynamic t in predictions) {
            items.add(AutoCompleteItem(
                id: t['place_id'],
                text: t['description'],
                offset: t['matched_substrings'][0]['offset'],
                length: t['matched_substrings'][0]['length']));
          }
        }
      }
    }).catchError((error) {
      print(error);
    });
    return items;
  }

  Future<LatLng?> decodeAndSelectPlace(String placeId) async {
    LatLng? position;
    String endpoint =
        "https://maps.googleapis.com/maps/api/place/details/json?key=${AppConfig.API_KEY}" +
            "&language=vi" +
            "&components=country:vn" +
            "&placeid=$placeId";

    await get(endpoint).then((response) {
      if (response.statusCode == 200) {
        Map<String, dynamic> location =
            response.body['result']['geometry']['location'];
        position = LatLng(location['lat'], location['lng']);
      }
    }).catchError((error) {
      print(error);
    });
    return position;
  }

  Future<LocationResult?> reverseGeocodeLatLng(LatLng latLng) async {
    LocationResult? locationResult;
    await get("https://maps.googleapis.com/maps/api/geocode/json?" +
            "latlng=${latLng.latitude},${latLng.longitude}&" +
            "language=vi&" +
//        "components=country:vn&"+
            "key=${AppConfig.API_KEY}")
        .then((response) {
      if (response.statusCode == 200) {
        Map<String, dynamic> responseJson = response.body;

        final result = responseJson['results'][0];
        print(result);

        String road = result['address_components'][0]['short_name'];
        String locality = result['address_components'][1]['short_name'];

        locationResult = LocationResult(
            name: road,
            locality: locality,
            latLng: latLng,
            formattedAddress: result['formatted_address'],
            placeId: result['place_id']);
      }
    }).catchError((error) {
      print(error);
    });
    return locationResult;
  }

  Future<Direction?> findDirection(
      LatLng initialPosition, LatLng finalPosition) async {
    Direction? direction;
    String directionUrl = "https://maps.googleapis.com/maps/api/directions/json"
        "?origin=${initialPosition.latitude},${initialPosition.longitude}"
        "&destination=${finalPosition.latitude},${finalPosition.longitude}&key=${AppConfig.API_KEY}";
    print(directionUrl);
    await get(directionUrl).then((response) {
      if (response.statusCode == 200 && response.body["routes"].length >0)
        direction = Direction(
            encodedPoints: response.body["routes"][0]["overview_polyline"]
                ["points"],
            distanceText: response.body["routes"][0]["legs"][0]["distance"]
                ["text"],
            distanceValue: response.body["routes"][0]["legs"][0]["distance"]
                ["value"],
            durationText: response.body["routes"][0]["legs"][0]["duration"]
                ["text"],
            durationValue: response.body["routes"][0]["legs"][0]["duration"]
                ["value"]);
    }).catchError((error) {
      print(error);
    });
    return direction;
  }
}
