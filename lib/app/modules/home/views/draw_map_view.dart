import 'package:flutter/material.dart';
import 'package:flutter_map/app/data/models/localtion_result.dart';
import 'package:flutter_map/app/modules/home/controllers/draw_map_controller.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class DrawMapView extends GetView<DrawMapController> {
  const DrawMapView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Vẽ đường đi'),
      ),
      body: Obx(() => Stack(
            children: [
              GoogleMap(
                padding: EdgeInsets.only(bottom: 300),
                mapType: MapType.none,
                myLocationButtonEnabled: true,
                initialCameraPosition: CameraPosition(
                  target: LatLng(10.8895353, 106.7739137),
                  zoom: 17,
                ),
                myLocationEnabled: true,
                zoomGesturesEnabled: true,
                zoomControlsEnabled: true,
                polylines: controller.polylineSet.value,
                onMapCreated: (GoogleMapController newController) {
                  controller.controllerGoogleMap.complete(newController);
                  controller.newGoogleMapController = newController;
                  controller.locatePosition();
                },
              ),
              Positioned(
                left: 0.0,
                right: 0.0,
                bottom: 0.0,
                child: Container(
                  height: 220.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(18),
                        topRight: Radius.circular(18.0)),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black,
                        blurRadius: 16.0,
                        offset: Offset(0.7, 0.7),
                        spreadRadius: 0.5,
                      ),
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 24.0, vertical: 18.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Xin chào",
                          style: TextStyle(fontSize: 12.0),
                        ),
                        Text(
                          "Bạn muốn đi đâu?",
                          style: TextStyle(
                              fontSize: 20.0, fontFamily: "Brand_Bold"),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        GestureDetector(
                          onTap: () async {
                            var data = await Get.toNamed('/home/find_place');
                            if (data != null) {
                              var dataLocation = data as LocationResult;
                              controller.setPickedLocation(dataLocation);
                            }
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(5.0),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black54,
                                  blurRadius: 6.0,
                                  offset: Offset(0.7, 0.7),
                                  spreadRadius: 0.5,
                                ),
                              ],
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.search,
                                    color: Colors.yellow,
                                  ),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  Text("Chọn vị trí cần tới "),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 24.0,
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.location_on,
                              color: Colors.grey,
                            ),
                            SizedBox(
                              width: 12.0,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(controller.pickPosition.value != null
                                    ? controller.pickPosition.value.toString()
                                    : 'Chưa chọn vị trí trên bản đồ'),
                                SizedBox(
                                  height: 4.0,
                                ),
                                InkWell(
                                  onTap: () => controller.drawDirections(),
                                  child: Text(
                                    "Đi tới vị trí đã chọn",
                                    style: TextStyle(
                                        color: Colors.black54, fontSize: 12.0),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
