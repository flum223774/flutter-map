import 'package:flutter_map/app/modules/home/controllers/find_map_controller.dart';
import 'package:flutter_map/app/widgets/search_input.dart';
import 'package:flutter_map/app/widgets/search_overlay.dart';
import 'package:flutter_map/app/widgets/select_place_action.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class FindPlaceView extends GetView<FindMapController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarSearch(
          child: SearchInput((text) => controller.searchPlace(text)),
        ),
        body: Obx(() => Column(
              children: [
                Expanded(
                    child: GoogleMap(
                  initialCameraPosition: CameraPosition(
                    target: LatLng(10.8895353, 106.7739137),
                    zoom: 15,
                  ),
                  myLocationButtonEnabled: false,
                  myLocationEnabled: true,
                  onMapCreated: controller.onMapCreated,
                  onTap: (latLng) {
                    controller.moveToLocation(latLng);
                  },
                  markers: controller.markers,
                )),
                !controller.hasSearchTerm.value
                    ? SizedBox()
                    : SizedBox(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SelectPlaceAction(controller.getLocationName(), () {
                              Get.back(result: controller.locationResult.value);
                            }),
                            Divider(
                              height: 8,
                            ),
                          ],
                        ),
                      )
              ],
            )));
  }
}
