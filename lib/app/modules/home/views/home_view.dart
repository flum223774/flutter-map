import 'package:flutter/material.dart';
import 'package:flutter_map/app/data/models/localtion_result.dart';
import 'package:geolocator/geolocator.dart';

import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('HomeView'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Center(
          child: Obx(() => Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Vị trí hiện tại:  ${controller.position.value?.latitude} -'
                    ' ${controller.position.value?.longitude}',
                    textAlign: TextAlign.center,
                  ),
                  ElevatedButton(
                      onPressed: controller.toggleListen,
                      child: Text('Lấy vị trí')),
                  Text(
                      'Vị trí lấy từ google map ${controller.positionPick.value?.latitude}'
                      ' -  ${controller.positionPick.value?.longitude}',
                      textAlign: TextAlign.center),
                  ElevatedButton(
                      onPressed: () async {
                        var data = await Get.toNamed('/home/find_place');
                        if (data != null) {
                          var dataLocation = data as LocationResult;
                          controller.setPickLocation(dataLocation.latLng);
                        }
                      },
                      child: Text('Lấy vị trí từ Map')),
                  ElevatedButton(
                      onPressed: controller.distanceCalculator,
                      child: Text(
                          'Khoảng cách chim bay ${controller.distance.value}',
                          textAlign: TextAlign.center)),
                  ElevatedButton(
                      onPressed: controller.distanceAPICalculator,
                      child: Text(
                          'Tính khoảng cách thực địa ${controller.distanceAPI}',
                          textAlign: TextAlign.center)),
                  ElevatedButton(
                      onPressed: () => Get.toNamed('/home/draw_map'),
                      child: Text('Vẽ đường'))
                ],
              )),
        ),
      ),
    );
  }
}
