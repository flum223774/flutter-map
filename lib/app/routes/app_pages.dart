import 'package:flutter_map/app/modules/home/bindings/draw_binding.dart';
import 'package:flutter_map/app/modules/home/bindings/find_binding.dart';
import 'package:flutter_map/app/modules/home/views/draw_map_view.dart';
import 'package:flutter_map/app/modules/home/views/find_place_view.dart';
import 'package:get/get.dart';

import 'package:flutter_map/app/modules/home/bindings/home_binding.dart';
import 'package:flutter_map/app/modules/home/views/home_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
        name: _Paths.HOME,
        page: () => HomeView(),
        binding: HomeBinding(),
        children: [
          GetPage(name: _Paths.FIND_PLACE, page: () => FindPlaceView(),
          binding: FindBinding()),
          GetPage(name: _Paths.DRAW_MAP, page: () => DrawMapView(),
          binding: DrawBinding())
        ]),
  ];
}
