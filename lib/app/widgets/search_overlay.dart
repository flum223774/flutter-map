import 'package:flutter/material.dart';
import 'package:get/get.dart';

final _tKey = GlobalKey(debugLabel: 'overlay_parent');
OverlayEntry? _loaderEntry;
bool _loaderShown = false;

class AutoCompleteItem {
  String? id;
  String text;
  int offset;
  int length;

  AutoCompleteItem({this.id,required this.text,required this.offset,required this.length});
}

class AppBarSearch extends StatelessWidget implements PreferredSizeWidget {
  final Widget child;

  const AppBarSearch({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: child,
      key: _tKey,
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(50.0);
}

class RichSuggestion extends StatelessWidget {
  final VoidCallback onTap;
  final AutoCompleteItem autoCompleteItem;

  RichSuggestion(this.autoCompleteItem, this.onTap);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        child: Container(
            padding: EdgeInsets.symmetric(
              horizontal: 24,
              vertical: 16,
            ),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: RichText(
                    text: TextSpan(children: getStyledTexts(context)),
                  ),
                )
              ],
            )),
        onTap: this.onTap,
      ),
    );
  }

  List<TextSpan> getStyledTexts(BuildContext context) {
    final List<TextSpan> result = [];

    String startText =
        this.autoCompleteItem.text.substring(0, this.autoCompleteItem.offset);
    if (startText.isNotEmpty) {
      result.add(
        TextSpan(
          text: startText,
          style: TextStyle(
            color: Colors.grey,
            fontSize: 15,
          ),
        ),
      );
    }

    String boldText = this.autoCompleteItem.text.substring(
        this.autoCompleteItem.offset,
        this.autoCompleteItem.offset + this.autoCompleteItem.length);

    result.add(TextSpan(
      text: boldText,
      style: TextStyle(
        fontSize: 15,
        color: Theme.of(context).textTheme.bodyText1!.color,
      ),
    ));

    String remainingText = this
        .autoCompleteItem
        .text
        .substring(this.autoCompleteItem.offset + this.autoCompleteItem.length);
    result.add(
      TextSpan(
        text: remainingText,
        style: TextStyle(
          color: Colors.grey,
          fontSize: 15,
        ),
      ),
    );

    return result;
  }
}

// OverlayState? get _overlayState {
//   final context = _tKey.currentContext;
//   if (context == null) return null;
//   NavigatorState? navigator;
//   void visitor(Element element) {
//     if (navigator != null) return;
//
//     if (element.widget is Navigator) {
//       navigator = (element as StatefulElement).state as NavigatorState;
//     } else {
//       element.visitChildElements(visitor);
//     }
//   }
//
//   context.visitChildElements(visitor);
//   print('navigator!.overlay ${navigator!.overlay}');
//   assert(navigator != null, '''unable to show overlay''');
//   return navigator!.overlay;
// }

Future<void> _showOverlay({required Widget child}) async {
  // print('OK $_overlayState');
  try {
    // final overlay = _overlayState;

    if (_loaderShown) {
      debugPrint('An overlay is already showing');
      return Future.value(false);
    }

    final overlayEntry = OverlayEntry(
      builder: (context) => child,
    );
    if(_tKey.currentContext != null){
      Overlay.of(_tKey.currentContext!)!.insert(overlayEntry);
    }
    // overlay!.insert(overlayEntry);
    _loaderEntry = overlayEntry;
    _loaderShown = true;
  } catch (err) {
    debugPrint('Exception inserting loading overlay\n${err.toString()}');
    throw err;
  }
}
Future<void> _hideOverlay() async {
  print('_loaderEntry $_loaderEntry');
  if(_loaderEntry != null){
    try {
      _loaderEntry!.remove();
      _loaderEntry = null;
      _loaderShown = false;
    } catch (err) {
      debugPrint('Exception removing loading overlay\n${err.toString()}');
      throw err;
    }
  }
}

Future<void> showAutoCompleteSearch() async {
  final RenderBox renderBox = Get.context!.findRenderObject() as RenderBox;
  Size size = renderBox.size;

  final RenderBox appBarBox =
      _tKey.currentContext!.findRenderObject() as RenderBox;
  try {
    await _showOverlay(
        child: Positioned(
      top: appBarBox.size.height,
      width: size.width,
      child: Material(
        elevation: 1,
        child: Container(
          padding: EdgeInsets.symmetric(
            vertical: 16,
            horizontal: 24,
          ),
          child: Row(
            children: <Widget>[
              SizedBox(
                width: 24,
              ),
              Expanded(
                child: Text(
                  "Finding place...",
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    ));
  } catch (err) {
    debugPrint('Exception showing loading overlay\n${err.toString()}');
    throw err;
  }
}

Future<void> showAutoCompleteSuggestions(
    List<RichSuggestion> suggestions) async {
  final RenderBox renderBox = Get.context!.findRenderObject() as RenderBox;
  Size size = renderBox.size;

  final RenderBox appBarBox =
      _tKey.currentContext!.findRenderObject() as RenderBox;
  try {
    await _showOverlay(
      child: Positioned(
        width: size.width,
        top: appBarBox.size.height,
        child: Material(
          elevation: 1,
          child: Column(
            children: suggestions,
          ),
        ),
      ),
    );
  } catch (err) {
    debugPrint('Exception showing loading overlay\n${err.toString()}');
    throw err;
  }
}

Future<void> hideAutoCompleteSuggestions() async {
  try {
    debugPrint('Hiding loading overlay');
    await _hideOverlay();
  } catch (err) {
    debugPrint('Exception hiding loading overlay');
    throw err;
  }
}